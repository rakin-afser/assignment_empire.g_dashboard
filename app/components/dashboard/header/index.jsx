import React,{Component} from 'react';
import {style} from './style'
import {css} from 'aphrodite';
import {Grid,Cell,Header} from 'react-mdl';
import {Badge} from 'react-mdl';

import Settings from './settings';
import odduu_logo from '../../../img/logo_odduu.png';

import { Link } from 'react-router';
export default class HeaderCom extends Component{
  constructor(props){
    super(props);

  }

  modifyIsOpen(){
    return this.setState({
      isOpen: !this.state.isOpen
    })
  }
  render(){
    var _that=this;
    return(
      <div >


          <Grid className={css(style.root)} noSpacing={true}>
            <Cell col={4}>
              <Grid noSpacing={true}>
                <Cell col={6} offset={3}><Link to='dashboard'><img src={odduu_logo} className={`${css(style.logo)}`}/></Link></Cell>
              </Grid>
            </Cell>
            <Cell col={4}>
                <h6>Empire Group</h6>
            </Cell>
            <Cell col={4}>
              <Grid noSpacing={true}>

                <Cell col={8} offset={2}>


                    <Grid >
                      <Cell  col={4}></Cell>
                      <Cell col={4}>    </Cell>
                      <Cell col={4}><Settings logout={this.props.logout}/></Cell>
                    </Grid>


                </Cell>
              </Grid>
            </Cell>
          </Grid>


      </div>
    )
  }
}
