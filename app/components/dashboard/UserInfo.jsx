import React from 'react';

class User_info extends React.Component {
render() {
return (
<div>
<div><b>User Role </b>: {this.props.role.toUpperCase()}</div>
<div><b>Created Date</b>: {`${new Date(this.props.createdDate).getFullYear()}-${new Date(this.props.createdDate).getMonth()+1}-${new Date(this.props.createdDate).getDate()}`}</div>
</div>

);
}
}

export default User_info;
