// @flow
import React, { Component } from 'react';
import HeaderContainer from './HeaderContainer';
import {Header, Drawer, Navigation,Layout,Content} from 'react-mdl';

export default class App extends Component {
  constructor(props){
    super(props);
  }

  render() {
    return (

      <div>
            {/* Header container */}
            <div>
                <HeaderContainer />
            </div>
            {/* sidebar & content container */}
            <div>
                {this.props.children}
            </div>

      </div>
    );
  }
}
