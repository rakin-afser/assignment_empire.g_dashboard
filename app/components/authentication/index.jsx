import React,{Component} from 'react';
import {Card, CardTitle, CardText, CardActions, Button,Textfield,Grid,Cell} from 'react-mdl';
import {css} from 'aphrodite';
import {Style} from './style';
import {Link} from 'react-router';
import FailureNotification from './failureNotification';
export default class LoginView extends Component {
  constructor(props) {
    super(props)
    this.state={
      userId:'',
      password:''
    }
    this.userIdChange=this.userIdChange.bind(this);
    this.passwordChange=this.passwordChange.bind(this);
    this.resetField=this.resetField.bind(this)


  }
  userIdChange(event){
    this.setState({
      userId:event.target.value.trim()
    })
  }
  passwordChange(event){
    this.setState({
      password:event.target.value.trim()
    })
  }
  resetField(){
    this.setState({userId:'', password:''})
  }

  render(){
    var _that=this;
    return (
      <div>
      <Card shadow={1} className={css(Style.root)}>
          <CardTitle className={css(Style.title)}>Empire Group</CardTitle>
          <CardText >
          <Textfield
            id='userId'
            ref='userId'
            value={this.state.userId}
            onChange={this.userIdChange}
            label="User ID"
            floatingLabel
            />
            <Textfield
              ref='password'
              value={this.state.password}
              onChange={this.passwordChange}
              type='password'
              label="password"
              floatingLabel
            />
            </CardText>
            <CardActions border>
              <Grid noSpacing={true}>
                <Cell col={2}>
                  <Button colored ripple className={css(Style.buttonLeft)}
                  onClick={()=>_that.props.submitLogin({userId:this.state.userId,password:this.state.password})

                  }>Login</Button>

                </Cell>

                <Cell col={2} offset={6}>
                  <Button colored ripple className={css(Style.buttonRight)}
                  onClick={this.resetField}>reset</Button>
                </Cell>
              </Grid>

            </CardActions>
      </Card>


       <FailureNotification failAttemps={this.props.failAttemps} />
      </div>
    )
  }
}
