var mori=require('mori');

export function setEntries(state, newState){
  console.log('into setEntries')
  return mori.into(state , mori.hashMap('entries',
        newState.reduce((vec,kv)=>{

                  return mori.conj(vec,kv);
                }, mori.vector())) );
}

export const initialStore=setEntries(mori.hashMap(),[]);

export function setAuthentication(state,data){
  if(data.isLoggedIn===false){
    return mori.assoc(state,'userData',data,'isLoggedIn',false,'failAttemps', mori.get(state,'failAttemps') != undefined ? mori.get(state,'failAttemps')+1 : 1 )
  }else{
    return mori.assoc(state,'userData',data,'isLoggedIn',true)
  }

}


export function logout(state){
  return mori.hashMap()
}
