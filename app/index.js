import configureStore from './store/configureStore'
import React from 'react';
import { render } from 'react-dom';
import { Provider } from 'react-redux';
import { Router, hashHistory ,Link,browserHistory} from 'react-router';
import { syncHistoryWithStore,push } from 'react-router-redux';
import io from 'socket.io-client'
import routes from './routes'
import {setAuthentication} from './actions/actions'

import 'react-mdl/extra/material.css';
import 'react-mdl/extra/material.js';
import mori from 'mori'
import * as path from './path';
import {createHistory} from 'history'

// socket.on('authenticationResult',function(obj){
//   var parsedObj=obj;
//
//   if(parsedObj.isLoggedIn===false){
//     store.dispatch(setAuthentication(parsedObj))
//   }else{
//
//     store.dispatch(setAuthentication(parsedObj));
//
//     pathChange(history,'dashboard')
//
//   }
// })

export function auth(){

  if(mori.toJs(store.getState().reducers).isLoggedIn === true){


  }else{

    pathChange('login')
  }
}
export function loginAuth(){

  if(mori.toJs(store.getState().reducers).isLoggedIn === true){
    pathChange('dashboard')

  }else{


  }
}
const store=configureStore(undefined);
const history=syncHistoryWithStore(hashHistory,store);

//Debugg--
store.subscribe(()=>{
  console.log('Debugging : Store -- ',store.getState())
})
//
render(
  <Provider store={store}>
    <Router history={history} routes={routes} />
  </Provider>,
  document.getElementById('root')
);

export function pathChange(pathName) {
  history.push(pathName)
}
