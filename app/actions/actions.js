// @flow

import * as _path from '../path';
import {pathChange} from '../index';
export function setState(state){
  return {
    type:'SET_ENTRIES',
    entries: state
    }
}



export function setAuthentication(obj){
  return {
    type:'SET_AUTHENTICATION',
    data:obj
  }
}
export function submitLogin(logInObj){
  let loginConfig =
  {
    method : 'POST',
    headers : { 'Content-Type':'application/x-www-form-urlencoded' ,'Access-Control-Allow-Origin':'*' },
    body: `userId=${logInObj.userId}&password=${logInObj.password}`,

  }

  return (dispatch) => {
    fetch(_path.login_url,loginConfig)
    .then(function(response){
      if(response.status === 200){
        return response.json()
      } else {
        dispatch(setAuthentication({isLoggedIn : false}))
      }


    })
    .then((data)=>{
      console.log(data);
      dispatch(setAuthentication(data.user))
      pathChange('dashboard')
    })

  }
}

export function defaultValue(check,value) {
  return (typeof check !== 'undefined' ? check : value)
}
export function createUser(name,id,password,role){
  var data={
    name:name,
    userId:id,
    password:password,
    role:role,
  }
  return {
    type : 'CREATE_USER',
    data:data,
  }
}
