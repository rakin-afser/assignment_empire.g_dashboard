// @flow
import React,{Component} from 'react';

import {Layout,Content} from 'react-mdl';
import {Link} from 'react-router';
import {Drawer,Navigation,HeaderRow} from 'react-mdl';
import User_info from './UserInfo';
export default class DashboardComponent extends Component {
  constructor(props) {
    super(props);
    console.log(this.props);
  }


  render(){
    const links = [];
    let menu=[  ];

    for (let ind = 0; ind <menu.length; ind++) {
      links.push(
        <Link key={ind} to={'dashboard/'+menu[ind].link} >{menu[ind].menu}</Link>
      )
    }
    var title=this.props.userData != undefined ? this.props.userData.name : 'User Found'

    return(
      <div>
        <Layout fixedDrawer={true}>


                <Drawer title={title.toUpperCase()} >

                  <Navigation>
                     {links}
                  </Navigation>
                </Drawer>
               <Content >
                  {this.props.children}
               </Content>
        </Layout>
      </div>
    )
  }
}
