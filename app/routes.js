import React from 'react';
import { Route, IndexRoute } from 'react-router';
import App from './containers/App';
import HomePage from './containers/HomePage';
import dashboard from './containers/dashboard';
import authentication from './containers/authentication';

import {auth, loginAuth} from './index';
export default (
  <Route path="/" name="Home" component={App}>


      <Route path='login' component={authentication} onEnter ={loginAuth}  ></Route>

      <Route path='dashboard' component={dashboard} onEnter={auth}  >
        <IndexRoute component={HomePage} ></IndexRoute>

      </Route>

    </Route>


);
